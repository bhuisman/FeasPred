import warnings
warnings.filterwarnings("ignore")
# from rdkit.Chem import AllChem, Descriptors, rdFMCS, MACCSkeys, Draw
import rdkit.Chem.rdChemReactions as RXN
import numpy as np
# from rdkit.Chem.Scaffolds import MurckoScaffold
# from rdkit.Chem.SaltRemover import *
# from rdkit.Chem.rdmolops import *
from rdkit import rdBase
from operator import itemgetter
from numpy import array, argmax
from keras.utils import to_categorical
from sklearn import preprocessing
import tensorflow as tf
from multiprocessing import Pool, cpu_count
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Embedding, Flatten, SimpleRNN, LSTM, CuDNNLSTM, Input
import pickle
rdBase.DisableLog('rdApp.*')
sess = tf.InteractiveSession()



indices_token = {'0': ' ', '1': '%', '10': '3', '11': '2', '12': '5', '13': '4', '14': '7', '15': '6',
                                  '16': '9', '17': '8', '18': '=', '19': 'A', '2': ')', '20': '@', '21': 'C', '22': 'B',
                                  '23': 'F', '24': 'I', '25': 'H', '26': 'K', '27': 'M', '28': 'L', '29': 'O', '3': '(',
                                  '30': 'N', '31': 'P', '32': 'S', '33': 'T', '34': 'V', '35': '[', '36': 'Z',
                                  '37': ']', '38': '\\', '39': 'a', '4': '+', '40': 'c', '41': 'e', '42': 'g',
                                  '43': 'i', '44': 'l', '45': 'o', '46': 'n', '47': 's', '48': 'r', '49': 't', '5': '-',
                                  '6': '/', '7': '.', '8': '1', '9': '0','50': 'p','51': 'b','52': 'd',
                                  '53': 'G', '54': 'E', '55': '#','56':'?', '57':'~'}

# labels = preprocessing.LabelEncoder()
# labels.fit(indices_token.values())

# def one_hot_encode(SMARTS):
#     mols = SMARTS.split('>>')
#     re = [mols[0].split('.')]
#     r1 = ''.join(tuple(re[0]))
#     max_len_in = 512
#     max_len_out = 256
#     try:
#         idxs_react = labels.transform([x for x in r1])
#         idxs_react = np.pad(idxs_react, (0, max_len_in-len(idxs_react)), 'constant')
#         idxs_prod = labels.transform([y for y in mols[1]])
#         idxs_prod = np.pad(idxs_prod, (0, max_len_out-len(idxs_prod)), 'constant')
#         # one_hot_react = tf.one_hot(idxs_react, 55, dtype=tf.uint8)
#         # one_hot_prod = tf.one_hot(idxs_prod, 55, dtype=tf.uint8)
#         # return one_hot_react.eval(), one_hot_prod.eval()
#         return idxs_react, idxs_prod
#     except:
#         pass

def one_hot_encode(SMARTS):
    # print(SMARTS)
    start = '?'
    end = '~'
    mols = SMARTS.split('>>')
    re = [mols[0].split('.')]
    max_len_in = 512
    max_len_out = 256
    idxs_react = []

    token_to_index = {v:int(k) for k,v in indices_token.items()}

    for reactant in re[0]:
        reactant = start+reactant+end
        # print (reactant)
        try:
        # reactant = labels.transform(reactant).tolist()
            reactant = [token_to_index[x] for x in list(reactant)]
            reactant.extend((max_len_out - len(reactant))*[0])
            idxs_react.extend(reactant)
        except:
            return None

    if len(idxs_react) != 512:
        return None

    product = mols[1]
    product = start+product+end
    # idxs_react = np.pad(idxs_react, (0, max_len_in-len(idxs_react)), 'constant')
    try:
        idxs_prod = [token_to_index[x] for x in list(product)]
        idxs_prod.extend((max_len_out-len(idxs_prod))*[0])

    except:
        return None

    if len(idxs_prod) != 256:
        return None
    # idxs_prod = np.pad(idxs_prod, (0, max_len_out-len(idxs_prod)), 'constant')
    # one_hot_react = tf.one_hot(idxs_react, 55, dtype=tf.uint8)
    # one_hot_prod = tf.one_hot(idxs_prod, 55, dtype=tf.uint8)
    # return one_hot_react.eval(), one_hot_prod.eval()
    return idxs_react, idxs_prod
    # except:
    #     pass

def prep_strings(reaction_SMARTS):

    reaction_SMARTS = str(reaction_SMARTS).replace(',\\n', '')
    if "\\n" not in reaction_SMARTS:
        try:
            reaction_SMARTS = str(reaction_SMARTS)
            reaction = reaction_SMARTS.split(',')[0]
            chars_stereochem = ['\\', '/', '@']
            reaction = reaction.replace('\\', '').replace('/', '').replace('@', '')
            product = reaction.split('>>')[1]
            reactants = reaction.split('>>')[0].split('.')
            reactants_out = []
            reaction = reaction.replace("b'", '')
            rxn = RXN.ReactionFromSmarts(reaction)
            rxn.Initialize()
            reaction_check = RXN.PreprocessReaction(rxn)
            if reaction_check[1] == 0:
                reaction = RXN.ReactionToSmarts(rxn)
                for mol in reactants:
                    if len(mol) < 6 and '[' in mol:
                        pass
                    else:
                        reactants_out.append(mol)
                if len(reactants_out) < 3 and len(reactants_out) > 1:
                    separator1 = '.'
                    react = separator1.join(reactants_out)
                    reaction_out = react+'>>'+product
                    return len(reaction_out), reaction_out
                else:
                    pass
        except:
            pass
    else:
        pass
cpu_num = cpu_count()
pool = Pool(processes=int(cpu_num*2)-3)

try:
    pkl_file = open("reactions.p", "rb")
    reactions = pickle.load(pkl_file)
    print('Reactions loading from pickle')
except:
    reactions = open('patent_test.txt', 'r')
    reaction_list = []
    print('Prepocessing datafile')
    # for mol in reactions:
    #     result = prep_strings(mol)
    result = pool.map(prep_strings, reactions)
    pool.close(), pool.join()

    result = filter(lambda x: x!= None, [x for x in result])

    [reaction_list.append(y) for y in result]

    reaction_list.sort(key=lambda x: x[0])
    cut_off = int(0.1*len(reaction_list))
    inv_cut_off = int(len(reaction_list)-cut_off)
    reactions = [str(react[1]) for react in reaction_list[cut_off:inv_cut_off]]
    pickle.dump(reactions, open("reactions_test.p", "wb"))

train_x = []
train_y = []
for z in reactions:
    res = one_hot_encode(z)
    if res == None:
        pass
    else:
        train_x.append(np.asarray(res[0]))
        train_y.append(np.asarray(res[1]))

train_x = np.vstack(train_x)
train_y = np.vstack(train_y)


print (train_x[0])
print(train_x.shape)
print(train_x[0].shape)

model = Sequential()
model.add(Embedding(58, 10, input_length=512))
# model.add(Flatten())
model.add(LSTM(512, dropout=0.3, return_sequences=True))
model.add(LSTM(256, dropout=0.2))
model.add(Dense(256))
model.add(Activation('softmax'))

model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
model.summary()
model.fit([train_x, train_y, epochs=50, batch_size=650)
