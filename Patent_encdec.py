from __future__ import print_function

import tensorflow as tf
from keras.models import Model, Sequential
from keras.layers import Input, LSTM, Dense
from keras.utils import to_categorical, np_utils, multi_gpu_model
from keras.callbacks import CSVLogger
import numpy as np
from numpy import array
import rdkit.Chem.rdChemReactions as RXN
from keras import regularizers
from rdkit import rdBase
import pickle
from sklearn.model_selection import train_test_split
from random import shuffle
from attention_decoder import AttentionDecoder

NUM_GPU = 1

rdBase.DisableLog('rdApp.*')
csv_logger = CSVLogger('FeasPred/log.csv', append=True, separator=';')
# batch_size = 100 # Batch size for training.
epochs = 5 # Number of epochs to train for.
latent_dim = 128  # Latent dimensionality of the encoding space.
input_dim = 512
num_samples = 200000  # Number of samples to train on.
generator_size = 40 * NUM_GPU
# Path to the data txt file on disk.
data_path = 'FeasPred/reactions.p'

def get_dataset(encoder_in, decoder_in, decoder_out, cardinality, batch):
    batch_features_enc = np.zeros((batch, input_dim, cardinality))
    batch_features_dec = np.zeros((batch, input_dim, cardinality))
    batch_features_dec_out = np.zeros((batch, input_dim, cardinality))

    while True:
        for e in range(batch):
            batch_features_enc[e] = to_categorical(encoder_in[e], num_classes=cardinality)
            batch_features_dec[e] = to_categorical(decoder_in[e], num_classes=cardinality)
            batch_features_dec_out[e] = to_categorical(decoder_out[e], num_classes=cardinality)
        yield [batch_features_enc, batch_features_dec], batch_features_dec_out

def get_validationset(encoder_in, decoder_out, cardinality, batch):
    batch_features_enc = np.zeros((batch, input_dim, cardinality))
    # batch_features_dec = np.zeros((batch, input_dim, cardinality))
    batch_features_dec_out = np.zeros((batch, input_dim, cardinality))

    while True:
        for e in range(batch):
            batch_features_enc[e] = to_categorical(encoder_in[e], num_classes=cardinality)
            # batch_features_dec[e] = to_categorical(decoder_in[e], num_classes=cardinality)
            batch_features_dec_out[e] = to_categorical(decoder_out[e], num_classes=cardinality)
        yield [batch_features_enc], batch_features_dec_out

def get_one_test(input_seq, cardinality):
    one_input_vec = np.zeros((input_dim, cardinality))
    try:
        for i, e in enumerate(input_seq[0]):
            one_input_vec[i] = to_categorical(e, num_classes=cardinality)
        return one_input_vec
    except:
        pass

def length_check(mol, max_input_length):
    text = str(mol)
    for i in range(max_input_length-len(mol)):
        text += '~'
    return str(text)

# Vectorize the data.
input_texts = []
target_texts = []
code_characters = set()
# target_characters = set()

pkl_file = open(data_path, "rb")
lines = pickle.load(pkl_file)
num_samples = len(lines)
    # lines = f.read().split('\n')
    # lines = filter(lambda x: x!= '', [x for x in lines])
    # lines = [y for y in lines]
# for line in lines[: min(num_samples, len(lines) - 1)]:
for line in lines:
    # line = line.replace('\\', '').replace('/', '').replace('@', '')
    line = line.split(',')[0]
    input_text, target_text = line.split('>>')
    input_text = input_text.split('.')
    input_t = ''
    for f, mol in enumerate(input_text):
        f = f + 1
        if f ==1:
            mol = '?'+mol
            input_t = length_check(mol, 256)
        if f == len(input_text):
            mol = mol+'~'
            input_t += length_check(mol, 256)
        # else:
        #     input_t += length_check(mol, 256)
    # We use "tab" as the "start sequence" character
    # for the targets, and "\n" as "end sequence" character.
    target_text = target_text.split('.')[0]
    target_text = '?' + target_text + '~'
    target_text = length_check(target_text, 512)
    input_texts.append(input_t)
    target_texts.append(target_text)
    for char in input_t:
        if char not in code_characters:
            code_characters.add(char)
    for char in target_text:
        if char not in code_characters:
            code_characters.add(char)


code_characters = sorted(list(code_characters))
# target_characters = sorted(list(target_characters))
num_encoder_tokens = len(code_characters)+1
# num_decoder_tokens = len(target_characters)+1
max_encoder_seq_length = 512
max_decoder_seq_length = 512

print('Number of samples:', len(input_texts))
print('Number of unique input tokens:', num_encoder_tokens)
print('Number of unique output tokens:', num_encoder_tokens)
print('Max sequence length for inputs:', max_encoder_seq_length)
print('Max sequence length for outputs:', max_decoder_seq_length)

input_token_index = dict(
    [(char, i) for i, char in enumerate(code_characters)])
target_token_index = dict(
    [(char, i) for i, char in enumerate(code_characters)])

encoder_input_data = []
# np.zeros((len(input_texts), max_encoder_seq_length, num_encoder_tokens), dtype='float32')
decoder_input_data = []
# np.zeros((len(input_texts), max_decoder_seq_length, num_decoder_tokens), dtype='float32')
decoder_target_data = []
# np.zeros((len(input_texts), max_decoder_seq_length, num_decoder_tokens), dtype='float32')

for i, (input_text, target_text) in enumerate(zip(input_texts, target_texts)):
    encode_in = []
    decode_in = []
    decode_target = []
    for t, char in enumerate(input_text):
        encode_in.append(input_token_index[char])
        # encoder_input_data[i, t, input_token_index[char]] = 1.
    for t, char in enumerate(target_text):
        # decoder_target_data is ahead of decoder_input_data by one timestep
        decode_in.append(target_token_index[char])
        # decoder_input_data[i, t, target_token_index[char]] = 1.
        if t > 0:
            decode_target.append(target_token_index[char])
            # decoder_target_data will be ahead by one timestep
            # and will not include the start character.
            # decoder_target_data[i, t - 1, target_token_index[char]] = 1.
    decode_target.append(6)
    if len(encode_in) == 512 and len(decode_in) == 512:
        encoder_input_data.append(np.asarray(encode_in))
        decoder_input_data.append(np.asarray(decode_in))
        # decode_target = decode_target.append()
        try:
            if len(decode_target) == 512:
                decoder_target_data.append(np.asarray(decode_target))
        except:
            pass

# encoder_input_data = np.vstack(encoder_input_data)
# decoder_input_data= np.vstack(decoder_input_data)
# decoder_target_data = np.vstack(decoder_target_data)
# encoder_input_data, decoder_input_data, decoder_target_data = get_dataset(encoder_input_data[:25000], decoder_input_data[:25000], decoder_target_data[:25000], max(num_encoder_tokens, num_decoder_tokens))
# encoder_input_data = np.squeeze(encoder_input_data, axis=1)
# decoder_input_data = np.squeeze(decoder_input_data, axis=1)
# decoder_target_data = np.squeeze(decoder_target_data, axis=1)
encoder_input_data, encoder_validation_data = train_test_split(encoder_input_data, test_size=0.2, random_state=42)
decoder_input_data, decoder_validation_data = train_test_split(decoder_input_data, test_size=0.2, random_state=42)
decoder_target_data, decoder_target_validation_data = train_test_split(decoder_target_data, test_size=0.2, random_state=42)

generator = get_dataset(encoder_input_data, decoder_input_data, decoder_target_data, num_encoder_tokens, generator_size)
validation_generator = get_dataset(encoder_validation_data, decoder_validation_data, decoder_target_validation_data, num_encoder_tokens, generator_size)

# Define an input sequence and process it.
encoder_inputs = Input(shape=(None, num_encoder_tokens))
# encoder = LSTM(latent_dim, return_sequences=True, dropout=0.5)(encoder_inputs)
encoder_outputs, state_h, state_c = LSTM(latent_dim, return_state=True, dropout=0.5)(encoder_inputs)
# encoder_outputs, state_h, state_c = encoder(encoder_inputs)
# We discard `encoder_outputs` and only keep the states.
encoder_states = [state_h, state_c]

# Set up the decoder, using `encoder_states` as initial state.
decoder_inputs = Input(shape=(None, num_encoder_tokens))
# We set up our decoder to return full output sequences,
# and to return internal states as well. We don't use the
# return states in the training model, but we will use them in inference.
decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True, dropout=0.75)
decoder_outputs, _, _ = decoder_lstm(decoder_inputs,
                                     initial_state=encoder_states)
# decoder_dense = AttentionDecoder(latent_dim, num_encoder_tokens)
decoder_dense = Dense(max(num_encoder_tokens, num_encoder_tokens), activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs)

# Define the model that will turn
# `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

# model = Sequential()
# model.add(LSTM(latent_dim, input_shape=(input_dim, num_encoder_tokens), return_sequences=True, dropout=0.75))
# model.add(AttentionDecoder(latent_dim, num_encoder_tokens))

if NUM_GPU != 1:
    # with tf.device("/cpu:0"):
        # model = model.build(input_shape=(None, num_encoder_tokens))
    model = multi_gpu_model(model, gpus=NUM_GPU)

print('Training started')
# print(get_dataset(encoder_input_data, decoder_input_data, decoder_target_data, max(num_encoder_tokens, num_decoder_tokens)).next)
# model.summary()

# Run training
model.compile(optimizer='adam', loss='categorical_crossentropy')
model.fit_generator(generator,
          steps_per_epoch=(len(encoder_input_data)/generator_size),
          epochs=epochs,
          validation_data=validation_generator,
          validation_steps=(len(encoder_validation_data)/generator_size),
          callbacks=[csv_logger]
          )
# Save model
model.save('FeasPred/models/s2s.h5')

# Next: inference mode (sampling).
# Here's the drill:
# 1) encode input and retrieve initial decoder state
# 2) run one step of decoder with this initial state
# and a "start of sequence" token as target.
# Output will be the next target token
# 3) Repeat with the current target token and current states

# Define sampling models
# encoder_model = Model(encoder_inputs, encoder_states)
#
# decoder_state_input_h = Input(shape=(latent_dim,))
# decoder_state_input_c = Input(shape=(latent_dim,))
# decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
# decoder_outputs, state_h, state_c = decoder_lstm(
#     decoder_inputs, initial_state=decoder_states_inputs)
# decoder_states = [state_h, state_c]
# decoder_outputs = decoder_dense(decoder_outputs)
# decoder_model = Model(
#     [decoder_inputs] + decoder_states_inputs,
#     [decoder_outputs] + decoder_states)

# Reverse-lookup token index to decode sequences back to
# something readable.
reverse_input_char_index = dict(
    (i, char) for char, i in input_token_index.items())
reverse_target_char_index = dict(
    (i, char) for char, i in target_token_index.items())


def decode_sequence(input_seq):
    # Encode the input as state vectors.
    try:
        states_value = encoder_model.predict(input_seq)

        # Generate empty target sequence of length 1.
        target_seq = np.zeros((1, 1, num_encoder_tokens))
        # Populate the first character of target sequence with the start character.
        target_seq[0, 0, target_token_index['?']] = 1.

        # Sampling loop for a batch of sequences
        # (to simplify, here we assume a batch of size 1).
        stop_condition = False
        decoded_sentence = ''
        while not stop_condition:
            output_tokens, h, c = decoder_model.predict(
                [target_seq] + states_value)

            # Sample a token
            sampled_token_index = np.argmax(output_tokens[0, -1, :])
            sampled_char = reverse_target_char_index[sampled_token_index]
            decoded_sentence += sampled_char

            # Exit condition: either hit max length
            # or find stop character.
            if (sampled_char == '~' or
               len(decoded_sentence) > max_decoder_seq_length):
                stop_condition = True

            # Update the target sequence (of length 1).
            target_seq = np.zeros((1, 1, num_encoder_tokens))
            target_seq[0, 0, sampled_token_index] = 1.

            # Update states
            states_value = [h, c]

        return decoded_sentence
    except:
        pass
#
#
def decode_input(sequence):
    decoded_sequence = ''
    for x in sequence:
        for y in x:
            sampled_token_index = np.argmax(y)
            sampled_char  = reverse_target_char_index[sampled_token_index]
            if sampled_char != '~':
                decoded_sequence = decoded_sequence+sampled_char
            else:
                return decoded_sequence
            if len(decoded_sequence) == 512:
                return decoded_sequence

test_file = 'test.txt'
with open(test_file, 'w') as out_file:
    for seq_index in range(5):
        # Take one sequence (part of the training set)
        # for trying out decoding.
        input_seq = encoder_input_data[seq_index: seq_index + 1]
        input_seq = get_one_test(input_seq, num_encoder_tokens)
        input_seq = np.expand_dims(input_seq, axis=0)
        decoded_sentence = decode_sequence(input_seq)
        out_file.write('Input sentence: {}\n'.format(input_texts[seq_index]))
        out_file.write('Decoded sentence: {}\n'.format(decoded_sentence))
    # print(decoded_sentence)
    # print('-')
    # print('Input sentence:', input_texts[seq_index])
    # print('Decoded sentence:', decoded_sentence)
